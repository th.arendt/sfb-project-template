# SFB Project Template

This project provides a template structure for studies within the SFB/CRR 135. The consistent structure has been designed for consistency across all projects in all labs. A single storage location allows for good data management while avoiding duplicate files. This can be edited as necessary for each unique need of an experiment.

# Description of Directories

**01_documentation**
  *  `11_regulatory` - Regulatory documents, e.g. ethics application forms or participant consent forms
  *  `12_methodology` - Documents describing the research methodology used, e.g., instructions for subjects or special equipment settings
  *  `13_datamanagement` - Documents describing the management of the research data in the project, e.g, data management plans, information on data file structure or naming document

**02_data**
  *  `21_rawdata` - Read-only, unprocessed data collected from a source (primary data) either in proprietary format or (open) standard format
  *  `22_processeddata` - Data obtained from the raw data during the analysis in order to gain information, e.g. filtered data or data transfered to an open format
  *  `23_finaldata` - Approved data in final reportable and stored format, e.g. aggregated data and plots

**03_code**
  *  `31_libraries` - Read-only, internal or external code libraries used during the study (either raw code or binary files)
  *  `32_studydesign` - Code that is used for running the experiments in order to collect the raw data
  *  `33_dataprocessing` - Code that is used for data processing, e.g. scripts for transforming from proprietary formats to open formats
  *  `34_dataanalysis` - Code that is used for analysing the data, e.g. scripts for producing plots or code for statistical analysis
  *  `35_modeling` - Code that is used for modeling purposes, e.g. scripts for defining neural networks 
 
**04_dissemination**
  *  `41_figures` - Figures produced during experiments and analysis
  *  `42_posters` - Posters prepared for presenting the study and/or its results
  *  `43_presentations` - Slides prepared for presenting the study and/or its results
  *  `44_manuscripts` - Articles prepared for presenting the study and/or its results
  *  `45_notebooks` - Jupyter notebooks used for prepared for presenting the study and/or its results

**05_misc** - All that does not fit to the structure above

# Sources

The file structure is based on and adapted from the folder organization for experiments used at [RITMO](https://www.uio.no/ritmo/english/research/labs/fourms/handbook/data-management/folder-structure-and-templates/file-structure/):

![folder organization for experiments at RITMO](https://www.uio.no/ritmo/english/research/labs/fourms/handbook/data-management/folder-structure-and-templates/file-structure/folderstructure.png)
